# Ejercicio 1. Crea los siguientes objetos de forma declarativa

Para este ejercicio, hemos decidido crear un deployment y un service (de tipo ClusterIP), ambos dentro de la carpeta ```exercise_1/kustomize```. Para crearlos, usamos el comando ```kubect apply```:

![k apply -f .\exercise_1\deployment.yaml,exercise1/service.yaml](exercise_1/screenshots/create.png)


## A continuación, se deberá acceder a la página principal de Nginx a través de la siguiente URL:

Para acceder desde el exterior, usamos un Ingress Controller, que expone rutas desde fuera del cluster apuntando a servicios de dentro del cluster:

![k apply -f .\exercise_1\ingress.yaml](exercise_1/screenshots/ingress.png)

En este apartado, hemos tenido problemas para acceder a minikube (que corre en WSL) desde Windows. Tras probar con distintos drivers (por ejemplo, el driver hiperkit no está soportado en windows/amd64, y el hyperv nos daba diversos problemas - hemos tenido que reinstalar minikube en dos ocasiones), hemos decicido simularlo usando el comando minikube tunnel:

![minikube tunnel](exercise_1/screenshots/minikube_tunnel.png)

Para acceder desde el navegador, creamos una ruta en el fichero de hosts de nuestro ordenador. En nuestro caso, hemos usado la aplicación SwitchHosts:

![SwitchHosts](exercise_1/screenshots/switchhosts.png)

De esta forma, ya podemos acceder al servicio desde el navegador a través de dicha url:

![Resultado final](exercise_1/screenshots/result.png)

También podemos acceder usando curl:

![Resultado final](exercise_1/screenshots/result_curl.png)

## Una vez realizadas las pruebas con el protocolo HTTP, se pide acceder al servicio mediante la utilización del protocolo HTTPS

Para crear el certificado con openssl hemos utilizado el comando sugerido por la documentación de Azure (https://docs.microsoft.com/en-us/azure/aks/ingress-own-tls#generate-tls-certificates):

![openssl req -x509 -nodes -days 365 -newkey rsa:2048 -out axel-afonso-tls.crt -keyout axel-afonso-tls.key -subj "/CN=axel.afonso.students.lasalle.com/O=axel-afonso-tls"](exercise_1/screenshots/certificate.png)

Para persistir el certificado en kubernetes usaremos un secret, pues un configMap lo guardaría en texto plano:

![k create secret tls axel-afonso-tls --key .\axel-afonso-tls.key --cert .\axel-afonso-tls.crt](exercise_1/screenshots/secret.png)

A continuación, creamos un nueco ingress con la configuración del certificado (exercise_1/ingress_tls.yaml):

![k apply -f ingress_tls.yaml](exercise_1/screenshots/ingress_tls.png)

Al acceder desde el navegador, nos sale la siguiente advertencia:

![Advertencia de certificado](exercise_1/screenshots/ingress_tls_warning.png)

Esto se debe a que nuestro certificado no está firnado por ninguna autoridad de certificación (CA) en la que confíe nuestro navegador (pueto que hemos creado un certificado autofirmado). Si aceptamos la advertencia, accedemos al servicio correctamente:

![Resultado final](exercise_1/screenshots/ingress_tls_result.png)

En producción, en lugar de crear un certificado autofirmado, solicitaríamos uno firmado por una CA de confianza.

En el navegador podemos ver el certifado creado:

![Certificado creado](exercise_1/screenshots/certificate_browser.png)