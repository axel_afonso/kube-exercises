# Ejercicio 3. Crea un objeto de kubernetes HPA, que escale a partir de las métricas CPU o memoria (a vuestra elección). Establece el umbral al 50% de CPU/memoria utilizada, cuando pase el umbral, automáticamente se deberá escalar al doble de replicas.

Para este ejercicio usaremos el mimso deployment (reduciendo el número de réplicas a 1 y la cpu - tanto en requests como en limits - a 10m) y el mismo service que para el ejercicio 1, que se pueden encontrar en ```exercise_3/deployment.yaml``` y en ```exercise_3/service.yaml```.

Cremaos el HPA con la especificación de ```exercise_3/hpa.yaml``` (la explicación de cada argumento está en dicho archivo).

![k apply -f .\exercise_3\ -R](exercise_3/screenshots/create.png)

Al cabo de un tiempo, debríamos ver que el hpa ya recibe las métricas de uso de cpu. Usando la apiVersion v1 del hpa, esto funciona correctamente, pero con la versión v2beta2 (necesaria para establecer las policies necesarias para que se duplique el número de pods) no hemos conseguido que funcione. El error es el siguiente:

![hpa error](exercise_3/screenshots/error.png)

Podemos ver que el hpa sí que escala el deployment hasta el mínimo número de réplicas, por lo que el error debe estar en el envío de las métricas desde los pods hasta el hpa.

Al correr el comando que aumenta la carga haciendo peticiones, vemos que (con el apiVersion v1) aumenta el número de pods, y al parar el proceso lo reduce. El comando utilizado es el siguiente:

    kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://php-apache; done"