# Ejercicio 2. Crear un StatefulSet con 3 instancias de MongoDB (ejemplo visto en clase)

Creamos el Statefulset según las especificaciones de ```exercise_2/statefulset.yaml```.
Para crear un cluster de MongoDB, además de crear el StatefulSet, necesitamos crear un "Headless Service" (```exercise_2/service.yaml```) para identificar los pods de mongo por nombre, en lugar de por su IP. Por lo tanto, creamos ambos usando ```k apply -f -R```:

![k apply -f .\exercise_2\ -R](exercise_2/screenshots/create.png)

El "Headless Service" no es más que un service de tipo ClusterIP (pues no neecesitamos acceder desde el exterior), y con ClusterIP igual a None (pues mo necesitamos que tenga ip).

De esta forma, podremos identificar la red de cada pod por ```<pod-name>.<service-name>```, donde ```<pod_name>``` se construye de la siguiente forma: ```<statefulset_name>-<pod_number>```. Como en nuestro caso tanto el StatefulSet como el service se llaman mongo, nuestros pods serán accesibles en la dirección ```mongo-0.mongo, mongo-1.mongo y mongo-2.mongo```.

## Habilitar el clúster de MongoDB

Para habilitar el cluster de MongoDB debemos acceder a mongo dentro de uno de los pods (por ejemplo, mongo-0):

![k exec -it pod/mongo-0 -- mongo](exercise_2/screenshots/exec_mongo.png)

A continuación, habilitamos el cluster (que en mongo se llama replicaset - rs) usando ```rs.initialize()```:

![rs.initiate()](exercise_2/screenshots/initiate.png)

Como podemos ver, nos dice que que no hay ninguna configuración especificada, por lo que usará la configuración por defecto. Para poder usar nuestros pods, especificamos una nueva configuración:

![Reconfiguring mongo replicaset](exercise_2/screenshots/reconfig.png)

La salida de ```rs.status()``` es demasiado extensa para incluirla en una captura, por lo que se encuentra en ```exercise_2/status.txt```. Ahí podemos ver los tres members creados, y sincronizándose.

## Realizar una operación en una de las instancias a nivel de configuración y verificar que el cambio se ha aplicado al resto de instancias

Para comprobar que está funcionando correctamente, creamos un usuario desde este mismo pod:

![db.createUser({user: "axel", pwd: "pass", roles: []})](exercise_2/screenshots/createUser.png)

Ahora, accedemos a otro pod (por ejemplo, mongo-1), y comrpobamos si se ha creado el mismo usuario ahí. Para ello, debemos ejecutar ```rs.secondaryOk()``` antes, que nos permite hacer operaciones a nivel de configuración desde un nodo que no sea el master:

![db.getUsers()](exercise_2/screenshots/getUsers.png).

## Diferencias que existiría si el montaje se hubiera realizado con el objeto de ReplicaSet

El principal problema que tendríamos al usar un ReplicaSet sería que el nombre de los pods no sería predecible (recordemos que en un replicaset el nombre de los pods se obtenía así: <replicaset_name>-<hash_aleatorio>). Por lo tanto, podríamos añadir los tres pods al replicaset de mongo, pero en cuanto se reiniciara un pod, el nuevo pod que se crearía automáticamente tendría un nobmre distinto, por lo que no pasaría a formar parte del cluster de mongo. 

Sin embargo, al usar un StatefulSet, si se cae el pod ```mongo-0```, el StatefulSet llamará ```mongo-0``` al nuevo pod que creee para sustituírlo, de forma que este pod ya pasaría a formar parte del cluster de mongo automáticamente.