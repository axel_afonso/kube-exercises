# Ejercicio 3. Crea un objeto de tipo service para exponer la aplicación del ejercicio anterior de las siguientes formas:

Durante todo este ejercicio, vamos a mantener corriendo el replicaset del ejercicio 2 para comprobar que los servicios están bien configurados:

![replicaset exercise 2](exercise_3/screenshots/replica_set.png)

Por otra parte, cabe destacar que todos los servicios que crearemos en este ejercicio realizan load balancing entre las réplicas de los pods a los que apuntan. Además, para especificar a qué pods apunta un service, usamos las labels de su especificación.

## Exponiendo el servicio hacia el exterior

Para exponer el servicio hacia el exterior, usaremos un servicio de tipo LoadBalancer. Típicamente haría falta un proveedor (AWS, Google Cloud, etc) para hacerlo, pero lo podmeos simular con minikube usando el comando ```minikube service```.
Creamos el service de forma descriptiva con el fichero ```exercise_3/serice_1.yaml``` usando ```kubectl apply```:

![k apply -f .\service_1.yaml](exercise_3/screenshots/service_1.png)

De esta forma, podemos acceder al servicio desde ```http://127.0.0.1:63228```. 

![nginx result](exercise_3/screenshots/service_1_result.png)

Si hubiéramos usado un proveedor, tendríamos acceso desde el exterior mendiante uan IP pública (podemos ver que ahora pone <pending>). Si se tratara de un frontend, solo faltaría añadir DNS para tener un servicio accesible para cualquier usuario.

## De forma interna, sin acceso desde el exterior

Para abrir un puerto al que solo se puede acceder desde el propio cluster de kubernetes, usaremos un service de tipo ClusterIp.

Creamos el service de forma descriptiva con el fichero ```exercise_3/serice_2.yaml``` usando ```kubectl apply```:

![k apply -f .\service_2.yaml](exercise_3/screenshots/service_2.png)

Ahora, cualquier aplicación de nuestro cluster puede acceder a nginx usando el servicio nginx-svc-2. Para probarlo, creamos un pod y ejecutamos ``curl`nginx-svc-2`` desde dentro:

![curl nginx-svc-2](exercise_3/screenshots/service_2_result.png)

Al ejecutar curl con el nombre del servicio, dependemos de que el puerto escogido sea el 80, pues es el puerto por defecto de curl. Para evitar esto, podemos usar las variables de entorno que kubernetes crea automáticamente al crear el pod, aunque hay que tener en cuenta que sólo crea dichas variables si creamos el pod después del servicio:

![curl nginx-svc-2](exercise_3/screenshots/service_2_env.png)

De esta forma, usaríamos ```curl NGINX_SVC_2_SERVICE_HOST:NGINX_SVC_2_SERVICE_PORT```.

## Abriendo un puerto especifico de la VM

Para abrir un puerto específico de la VM usaremos un service del tipo NodePort.

Creamos el service de forma descriptiva con el fichero ```exercise_3/serice_3.yaml``` usando ```kubectl apply```:

![k apply -f .\service_3.yaml](exercise_3/screenshots/service_3.png)

De esta forma, podemos acceder a nginx desde la dirección ```192.168.49.2:30351```, donde ```192.168.49.2``` es la ip del nodo (minikube en nuestro caso), y ```30351``` es el puerto abierto en dicho nodo por el servicio (escogido de entre un rango de puertos - por defecto 30000-32767).

Si quisiéramos usar un puerto concreto, podríamos añadir el campo ```nodePort``` en ```service_3.yaml```, pero correríamos el riesgo de que el servicio fallara por estar dicho puerto ocupado.