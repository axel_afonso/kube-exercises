# Ejercicio 4. Crear un objeto de tipo deployment con las especificaciones del ejercicio 1

Para ejemplificar las versiones de la aplicación, creamos una imangen con ```exercise_4/Dockerfile.<version>``` en la que copiamos un ```index_<version>.html``` diferente para cada versión (el index.html muestra la versión actual).

## Despliega una nueva versión de tu nuevo servicio mediante la técnica “recreate”

Creamos un deployment de forma declarativa con ```exercise_4/deployment_recreate.yaml```, y acutalizamos la versión del deployment con ```k set image```:

![k apply -f .\deployment_recreate.yaml && k set image deploy/nginx-recreate nginx-app=axelafonsoblanco/version2](exercise_4/screenshots/deployment-recreate.png)

Como podemos ver, se eliminan los pods de la versión 1 antes de crearse los de la versión 2:

![Recreate result](exercise_4/screenshots/deployment-recreate-result.png)

Para ver el progreso del cambio de las imágenes usamos el siguiente comando (en PowerShell):

    while($true) {`
      k get pods -o go-template --template="{{range .items}}{{.metadata.name}} -> {{range .status.containerStatuses}}{{.image}}`t{{end}}{{end}}`n";`
      sleep 1;`
    }

![Recreate images](exercise_4/screenshots/deployment-recreate-images.png)

Esto implica que tenemos un downtime entre que se elimina el último pod viejo y se crea el último pod nuevo.

## Despliega una nueva versión haciendo “rollout deployment”

Creamos un deployment de forma declarativa con ```exercise_4/deployment_rollout.yaml```, y acutalizamos la versión del deployment con ```k set image```:

![k apply -f .\deployment_rollout.yaml && k set image deploy/nginx-recreate nginx-app=axelafonsoblanco/version2](exercise_4/screenshots/deployment-rollout.png)

Como podemos ver, en este caso no se elimina ningún pod hasta que hay otro de la nueva versión funcionando:

![Rollout result](exercise_4/screenshots/deployment-rollout-result.png)
![Rollout images](exercise_4/screenshots/deployment-rollout-images.png)

Concretamente, se empieza a crear un pod nuevo, y cuando está creado se empieza a eliminar uno viejo y a crear un segundo pod nuevo (ambas cosas ocurren a la vez). En cuanto se crea el segundo pod nuevo, empieza a eliminarse un segundo pod viejo y a crearse el tercero nuevo, y así sucesivamente. Cabe destacar que en este caso se considera que un pod está creado cuando empieza a ejecutarse, pero si tuviéramos configurada una ```readinessProbe``` no se consideraría creado hasta que esa prueba se completara correctamente.

De esta forma, eliminamos el downtime, pero tenemos pods de dos versiones distintas fucnionando a la vez.


## Realiza un rollback a la versión generada previamente

Para hacer un rollback a la versión generada previamente, usamos ```k rollout undo```:

![k rollout undo deploy nginx-rollout](exercise_4/screenshots/deployment-rollback.png)

Como la estrategia del deployment es rollout ("RollingUpdate"), el rollback se hace con la misma estrategia:

![Rollback result](exercise_4/screenshots/deployment-rollback-result.png)
![Rollback images](exercise_4/screenshots/deployment-rollback-images.png)