# Ejercicio 5. Diseña una estrategia de despliegue que se base en ”Blue Green”. Podéis utilizar la imagen del ejercicio 1

Para este ejercicio usaremos las mismas imágenes del ejercicio anterior (Ejercicio 4).

Para realizar el despliegue con estrategia "Blue Green" usaremos dos deployments, ```nginx-blue``` y ```nginx-green``` y dos servicios, ```nginx-prod``` y ```nginx-test```. Cabe destacar que usaremos la estrategia "Recreate" en cada deployment para ahorrar recursos, pues cuando cambiemos un deployment de versión no va a haber ningún servicio apuntando a dicho deployment, por lo que no tiene sentido hacer un rolling update.

En un principio, solo se está ejecutando el deployment ```nginx-blue``` con la imagen ```axelafonsoblanco/exercise4:1.0```, el servicio ```nginx-prod``` apuntando al deployment ```nginx-blue```. Para apuntar a uno u otro deployment usaremos el selector ```color=blue``` y ```color=green``` respectivamente, además del ```app=nginx-server``` que tienen ambos deployments.

![Both blue](exercise_5/screenshots/initial.png)

En cuanto tenemos la imanen ```axelafonsoblanco/exercise4:2.0```, creamos el deployment ```nginx-green``` con dicha imagen, y el servicio ```nginx-test``` apuntando a dicho deployment:

![Blue and green](exercise_5/screenshots/blue-and-green.png)

En este momento, tenemos acceso a la versión 1.0 a través de ```nginx-prod```, y podemos realizar los test necesarios en la versión 2.0 a través de ```nginx-test```.

Una vez finalizan los tests, pasamos producción a la nueva versión, cambiando el selector del servicio ```nginx-prod``` (con el comando ```kubectl set selector```). De esta forma, cambiamos de versión con 0 downtime y sin tener en ningún momento dos versiones distintas accesibles en producción a la vez.

![Both green](exercise_5/screenshots/both-green.png)

Ahora ambos servicios están apuntando al deployment ```nginx-green```, con la versión 2.0. Para ahorrar recursos, eliminamos el servicio de test y escalamos el deployment ```nginx-blue``` a 0 réplicas. Podríamos eliminarlo, pero al escalarlo a 0 réplicas lo tenemos listo para volver a cambiar de versión, que en el siguiente cambio se haría en sentido inverso (ahora lo hicimos de blue a green y la próxima vez se haría de green a blue).

De esta forma, el resultado final es el siguiente:

![Both green](exercise_5/screenshots/final.png)
