# Ejercicio 1. Crear un pod

Para consultar los parámetros de la declaración de objetos en kubernetes, podemos consultar la documentación de kubernetes. En concreto, usaremos el apartado de pods, que se puede encontrar en el siguiente enlace: https://kubernetes.io/es/docs/concepts/workloads/pods/. Para obtener información más detallada, consultaremos la referencia: https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/pod-v1/.

La declaración creada se encuentra en la carpeta exercise_1 (fichero pod.yaml). La explicación de los parámetros utilizados se explica con comentarios en dicho archivo.

Para crear el pod basándonos en dicho fichero, ejecutamos el siguiente comando (desde la carpeta exercise_1):

    kubectl apply -f ./pod.yaml --force

Podemos usar --force porque en este ejercicio no se está accediendo a ninguna base de datos, así que no necesitamos que se espere ningún grace-period cuando eliminamos el pod.


## ¿Cómo puedo obtener las últimas 10 líneas de la salida estándar (logs generados por la aplicación)?

Para consultar los comandos de kubectl, lo más rápido es ver la referencia de kubectl. El apartado de logs se puede encontrar en este enlace: https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands#logs.

Según la referencia de kubectl, el comando a usar será ```logs```:

![Logs](exercise_1/screenshots/logs.png)

En nuestro caso, sólo obtenemos 7 líneas, porque el pod no ha imprimido más logs. Si hubiera imprimido más logas sóloberíamos las últimas 10 líneas.

## ¿Cómo podría obtener la IP interna del pod? Aporta capturas para indicar el proceso que seguirías.

Para obtener la ip de un pod podemos usar el comando ```describe``` y buscar la IP entre todos los resultados:

![Pod ip con describe](exercise_1/screenshots/pod_ip_describe.png)


Sin embargo, resulta mucho más cómodo utilizar el comando ```get``` con la opción ```-o wide```

![Pod ip con get](exercise_1/screenshots/pod_ip_get.png)


## Necesitas visualizar el contenido que expone NGINX, ¿qué acciones debes llevar a cabo?

Para visualizar el contenido de un pod sin crear un servicio, lo más rápido es usar el comando port-forward de kubectl:

![kubectl port-forward nginx-app 8080:80](exercise_1/screenshots/port-forward-command.png)

En este comando, el primer puerto es el de la máquina host, y el segundo es el del port (podemos ver que el propio kubctl nos indica la ip y el puerto en el que ahora podemos acceder desde el host).

Al acceder a esa dirección desde el navegador, podemos ver el resultado:

![port-forward result](exercise_1/screenshots/port-forward-result.png)


## Indica la calidad de servicio (QoS) establecida en el pod que acabas de crear. ¿Qué lo has mirado?

Como al crear el pod espeficícamos limits y requests iguales tanto para memoria como para cpu, la calidad de servicio del pod debería ser "Guaranteed". Si hubiéramos asignado limits mayor que requests, tendríamos un pod con QoS "Burstable", y si no hubiéramos asignado ni limits ni requests tendríamos un QoS "Best effort".

Para ver la calidad de servicio del pod, podemos usar tanto el comando ```k get pod``` como el comando ```k describe pod```. Para no tener que buscarlo entre todas las opciones, vamos a usar el primero, junto con la opción ```--output```, para mostrar únicamente la calidad de servicio:

![k get pod nginx-app --output='jsonpath="{.status.qosClass}"'](exercise_1/screenshots/qos.png)

Otra fomra de filtrar lo que se muestra es --template, que usaremos más adelante en estos ejercicios.