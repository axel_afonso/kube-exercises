# Ejercicio 2

Creamos el ReplicaSet de forma descriptiva con el fichero ```exercise_2/replica_set.yaml``` usando ```kubectl apply```:

![k apply -f .\replica_set.yaml](exercise_2/screenshots/replicaset.png)

## ¿Cúal sería el comando que utilizarías para escalar el número de replicas a 10?

Para escalar el número de réplicas a 10, usamos ```kubectl scale```:

![k scale rs nginx-app --replicas==10](exercise_2/screenshots/replicas_10.png)

## Si necesito tener una replica en cada uno de los nodos de Kubernetes, ¿qué objeto se adaptaría mejor? (No es necesario adjuntar el objeto)

Para tener una réplica en cada uno de los nodos, usaríamos DaemonSet (https://kubernetes.io/es/docs/concepts/workloads/controllers/daemonset/).

Con un DaemonSet podemos garantizar que se ejecuta una réplica en cada nodo (o definir en qué nodos concretos queremos ejecutar réplicas). Como indica la documentación de kubernetes, se suele utilizar para monitorización, recolección de logs o procesos de almacenamiento (entre otras cosas), pues es algo que se debe hacer en todos los nodos.
